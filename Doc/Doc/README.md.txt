CN
S3-勘误表：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_errata_cn.pdf
S3-技术规格书：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_datasheet_cn.pdf
S3-技术参考手册：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_technical_reference_manual_cn.pdf
S3-硬件设计指南：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_hardware_design_guidelines_cn.pdf
EN
S3-Errata：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_errata_en.pdf
S3-Datasheet：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_datasheet_en.pdf
S3-Technical Reference Manual：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_technical_reference_manual_en.pdf
S3-Hardware Design Guidelines：https://www.espressif.com.cn/sites/default/files/documentation/esp32-s3_hardware_design_guidelines_en.pdf

ESP-IDF:HTTPS://GITHUB.COM/ESPRESSIF/ESP-IDF

IF YOU HAVE ANY QUESTIONS, YOU CAN CONTACT WeAct Studio.