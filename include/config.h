#ifndef __ATOM_CONFIG_H
#define __ATOM_CONFIG_H

/* 01. 内核配置 */

#define ATOM_USE_IDLE_HOOK

#define ATOM_USE_KERNEL_DBG (1)

#if ATOM_USE_KERNEL_DBG  
    #define ATOM_STACK_CHECKING
    //@note 启动本功能将占用更多的堆栈，console堆栈需要大于256字节
    #define ATOM_TSTATUS_CHECKING
#endif

#define ATOM_PROIORITY_MAX   (23)
/*
 * Idle thread stack size
 *
 * This needs to be large enough to handle any interrupt handlers
 * and callbacks called by interrupt handlers (e.g. user-created
 * timer callbacks) as well as the saving of all context when
 * switching away from this thread.
 */

#define ATOM_IDLE_STACK_SIZE_BYTES       (2048)

/*
 * Main thread stack size
 *
 * Note that this is not a required OS kernel thread - you will replace
 * this with your own application thread.
 *
 * In this case the Main thread is responsible for calling out to the
 * test routines. Once a test routine has finished, the test status is
 * printed out on the UART and the thread remains running in a loop.
 *
 * The Main thread stack generally needs to be larger than the idle
 * thread stack, as not only does it need to store interrupt handler
 * stack saves and context switch saves, but the application main thread
 * will generally be carrying out more nested function calls and require
 * stack for application code local variables etc.
 *
 * 1KB might be adequate but if using printf() then at least 2KB would be
 * prudent otherwise the stdio functions otherwise stack overruns are
 * likely. Nearly 2KB was seen to be used on the toolchain used for
 * development.
 */

#define ATOM_MAIN_STACK_SIZE_BYTES       (2048)



/* 02. 调试相关 */
// 是否使用自定义printf()，c的printf将占用比较多的堆栈，资料少的设备不推荐启动
#define COM_USE_MYPRINTF
    #ifdef COM_USE_MYPRINTF
    // 如果需要打印更多信息，需要加大buffer，但会占用更多的堆栈
    #define COM_MYPRINTF_BUFFER (128)
#endif //COM_USE_MYPRINTF

#define COM_USE_DBGPORT
#ifdef COM_USE_DBGPORT
    #define BSP_DBGPORT_RX_BUFSIZE (1024)
    #define BSP_DBGPORT_DEVICE_NAME  "serial"
    #define COM_USE_DBG_COLOR
    #define COM_USE_KASSERT
    #define COM_USE_SYSLOG
    #define COM_SYSLOG_MASK_LEVEL LOG_INFO
#endif   

#define COM_USE_CONSOLE
// console task stack size for stm8
#ifdef COM_USE_CONSOLE
    #define COM_CONSOLE_STACK_SIZE_BYTES (2048)
    #define COM_PROIORITY_CONSOLE (ATOM_PROIORITY_MAX-2)
    #define COM_CONSOLE_HISTORY (5)
    #define COM_CONSOLE_HELPINFO
    #define COM_CONSOLE_PREFIX_SPACE (20)
    #define COM_CONSOLE_CMD_MAXLEN (80)
    #define COM_CONSOLE_MAX_PARAM_COUNT (10)
    /* shell command config */
    #define COM_USE_DBG_CMD
    #define COM_USE_SHELL_CMD
    #define COM_USE_PIN_CMD
    #define COM_USE_RTC_CMD
    #define COM_USE_ADC_CMD
    #define COM_USE_PWM_CMD
    #define COM_INTERRUPT_NAME_MAX (80)
    #define COM_USING_INTERRUPT_INFO

#endif //COM_USE_CONSOLE

/* 03. 用户配置 */
#define APP_NAME "esp32-lyrat maple environment"
#define APP_VERSION "1.0.0"


/* 04. HW相关 */

#ifdef BSP_ROM_RAM_CHECK
    #define BSP_ROM_START              ((uint32_t)0x00000000)
    #define BSP_ROM_SIZE               (0 * 1024)
    #define BSP_ROM_END                ((uint32_t)(BSP_ROM_START + BSP_ROM_SIZE))

    #define BSP_RAM_START              (0x00000000)
    #define BSP_RAM_SIZE               (0 * 1024 * 1024 * 1024)
    #define BSP_RAM_END                (BSP_RAM_START + BSP_RAM_SIZE)
#endif

#define BSP_USE_PIN
#define BSP_USE_RTC
#define BSP_USE_SIMSPI
#define BSP_USE_UART
#define BSP_USE_SPI
#define BSP_USE_ADC
#define BSP_USE_PWM
#define BSP_USE_SERIAL
#define BSP_USE_SERIAL1
#define BSP_USE_SERIAL2
#define BSP_USE_SIMIIC
#define BSP_USE_IIC
#define BSP_USE_LEDS

#define BSP_USE_ADC
#ifdef BSP_USE_ADC
#define BSP_USE_ADC1
#define BSP_USE_ADC2
#endif

#define BSP_USE_WIFI_CONSOLE

/* 05. devices */
#define DEVICE_USE_IAP
#define DEVICE_IAP_NAME "iap"
#define DEVICE_IAP_IOS_NAME "serial1"

#define DEVICE_PROPERTY_LEN (20)
#define DEVICE_DRV_NAME_LEN  (10)
#define DEVICE_PV_LEN (64)
#define DEVICE_USE_KEYBOARD
#define DEVICE_KEYBOARD_NAME "keyboard"
#define DEVICE_USE_GNSS
#define DEVICE_GNSS_NAME    "gnss"
#define DEVICE_GNSS_IO      "serial2"

#endif